"""marketplace URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
""" 
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from organic.views import HomePageView, Detail, About, Contact, ArticleView, DetailArticle, GaleryView

urlpatterns =patterns ('',
    url(r'^$', HomePageView.as_view(), name='homepageview'),
    url(r'^kontak/$', Contact.as_view(), name='contact'),
    url(r'^tentang/$', About.as_view(), name='about'),
    url(r'^artikel/$', ArticleView.as_view(), name='article'),
    url(r'^galeri/$', GaleryView.as_view(), name='galery'),
 
    url(r'^produk/detail/(?P<slug>[\w\d\-\.]+)$', Detail.as_view(), name='detail'),
    url(r'^detailartikel/(?P<slug>[\w\d\-\.]+)$', DetailArticle.as_view(), name='detailarticle'),
    #url(r'^organik/(?P<slug>[\w\d\-\.]+)$', Organik.as_view(), name='organik'),
                    
    url(r'^tinymce/', include('tinymce.urls')),   
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                 {'document_root': settings.MEDIA_ROOT}),

)

admin.autodiscover()
urlpatterns += patterns(
    '',
    url(r'^admin/', include(admin.site.urls)),
)
