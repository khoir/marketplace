from django import template
from organic.models import Product
register = template.Library()

def get_beras(beras):
    data = Product.objects.only("slug", "name").all()
    return data


register.filter('get_beras', get_beras)
