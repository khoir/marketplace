# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('unique_id', models.CharField(unique=True, max_length=255)),
                ('creation_date', models.DateTimeField(null=True, verbose_name=b'creation date', blank=True)),
                ('checked_out', models.BooleanField(default=False, verbose_name=b'checked out')),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.PositiveIntegerField(default=0)),
                ('unit_price', models.DecimalField(null=True, max_digits=18, decimal_places=2, blank=True)),
                ('cart', models.ForeignKey(verbose_name=b'cart', to='organic.Cart')),
                ('product', models.ForeignKey(to='organic.Product')),
            ],
        ),
    ]
