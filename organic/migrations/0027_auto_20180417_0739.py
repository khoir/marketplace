# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0026_auto_20180417_0738'),
    ]

    operations = [
        migrations.DeleteModel(
            name='History',
        ),
        migrations.DeleteModel(
            name='Organik',
        ),
    ]
