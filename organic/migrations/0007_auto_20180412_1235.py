# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0006_product_slug'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bank',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('photo', models.ImageField(upload_to=b'produk', null=True, verbose_name=b'Photo', blank=True)),
                ('no_rek', models.CharField(max_length=20, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ListDelivery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('address', models.CharField(max_length=200, null=True, blank=True)),
                ('phone', models.CharField(max_length=20, null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='cart',
            name='token',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='cart',
            name='user',
            field=models.ForeignKey(blank=True, to='userapp.UserProfile', null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.TextField(help_text=b'Deskripsi produk organik blablabla', null=True, verbose_name=b'deskripsi', blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='photo',
            field=models.ImageField(upload_to=b'produk', null=True, verbose_name=b'Photo', blank=True),
        ),
    ]
