# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0008_auto_20180412_1322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='listdelivery',
            name='receiver',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
