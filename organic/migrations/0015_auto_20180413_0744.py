# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0014_auto_20180412_1440'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='listdelivery',
            name='receiver',
        ),
        migrations.AddField(
            model_name='listdelivery',
            name='cart',
            field=models.ForeignKey(default=1, verbose_name=b'cart', to='organic.Cart'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='listdelivery',
            name='creation_date',
            field=models.DateTimeField(null=True, verbose_name=b'creation date', blank=True),
        ),
        migrations.AddField(
            model_name='listdelivery',
            name='kota',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='listdelivery',
            name='provinsi',
            field=models.CharField(max_length=20, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='listdelivery',
            name='statustransfer',
            field=models.BooleanField(default=False, verbose_name=b'checked out'),
        ),
        migrations.AddField(
            model_name='listdelivery',
            name='totalharga',
            field=models.DecimalField(null=True, max_digits=18, decimal_places=2, blank=True),
        ),
        migrations.AddField(
            model_name='listdelivery',
            name='totalitem',
            field=models.DecimalField(null=True, max_digits=18, decimal_places=2, blank=True),
        ),
    ]
