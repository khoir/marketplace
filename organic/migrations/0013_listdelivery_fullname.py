# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0012_auto_20180412_1345'),
    ]

    operations = [
        migrations.AddField(
            model_name='listdelivery',
            name='fullname',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
