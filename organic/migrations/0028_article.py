# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import organic.models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0027_auto_20180417_0739'),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('slug', models.CharField(max_length=350, unique=True, null=True, blank=True)),
                ('shortdesc', models.CharField(max_length=300, null=True, blank=True)),
                ('photo', models.ImageField(upload_to=organic.models.get_article_upload, null=True, verbose_name=b'Photo', blank=True)),
            ],
        ),
    ]
