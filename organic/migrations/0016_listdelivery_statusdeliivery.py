# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0015_auto_20180413_0744'),
    ]

    operations = [
        migrations.AddField(
            model_name='listdelivery',
            name='statusdeliivery',
            field=models.BooleanField(default=False, verbose_name=b'checked out'),
        ),
    ]
