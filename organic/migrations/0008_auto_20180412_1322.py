# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userapp', '0001_initial'),
        ('organic', '0007_auto_20180412_1235'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bank',
            name='name',
        ),
        migrations.RemoveField(
            model_name='listdelivery',
            name='name',
        ),
        migrations.AddField(
            model_name='bank',
            name='Name_receiver',
            field=models.ForeignKey(blank=True, to='organic.ListDelivery', null=True),
        ),
        migrations.AddField(
            model_name='listdelivery',
            name='receiver',
            field=models.ForeignKey(blank=True, to='userapp.UserProfile', null=True),
        ),
    ]
