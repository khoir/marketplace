# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0004_auto_20180411_0347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='user',
            field=models.ForeignKey(default='2', verbose_name=b' user', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
    ]
