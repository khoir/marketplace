# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0020_bank_onbehalf'),
    ]

    operations = [
        migrations.CreateModel(
            name='Organik',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('slug', models.CharField(max_length=300, unique=True, null=True, blank=True)),
                ('description', models.TextField(help_text=b'Deskripsi produk organik blablabla', null=True, verbose_name=b'deskripsi', blank=True)),
                ('photo', models.ImageField(upload_to=b'produk', null=True, verbose_name=b'Photo', blank=True)),
                ('active', models.BooleanField(default=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
