# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organic', '0016_listdelivery_statusdeliivery'),
    ]

    operations = [
        migrations.AddField(
            model_name='listdelivery',
            name='nbank',
            field=models.CharField(max_length=50, null=True, blank=True),
        ),
    ]
