# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('full_name', models.CharField(max_length=100, null=True, blank=True)),
                ('address', models.CharField(max_length=200, null=True, blank=True)),
                ('phone', models.CharField(max_length=20, null=True, blank=True)),
                ('email', models.CharField(max_length=100, null=True, blank=True)),
                ('dob', models.DateTimeField(null=True, blank=True)),
                ('role', models.CharField(max_length=2, choices=[(b'1', b'admin'), (b'2', b'customer')])),
                ('joined', models.DateTimeField(auto_now_add=True)),
                ('login', models.DateTimeField(null=True, blank=True)),
                ('logout', models.DateTimeField(null=True, blank=True)),
                ('photo', models.ImageField(upload_to=b'profilepicture', null=True, verbose_name=b'Photo', blank=True)),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
