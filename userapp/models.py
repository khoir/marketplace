from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    full_name = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=200, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    dob = models.DateTimeField(blank=True, null=True) #date of birth

    ROLE = (('1', 'admin'), ('2', 'customer'))
    role = models.CharField(max_length=2, choices=ROLE)
    
    joined =  models.DateTimeField(auto_now_add=True)
    login = models.DateTimeField(blank=True, null=True)
    logout = models.DateTimeField(blank=True, null=True)
    
    PHOTOS_DIR_NAME = "profilepicture"
    photo = models.ImageField('Photo', upload_to=PHOTOS_DIR_NAME, blank=True, null=True)
    
    def __str__(self):
        return self.full_name
       

